#include <iostream>

#include <algorithm>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

int main() {
    int sockfd, newsockfd, portno, clilen, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    std::cout << "Hello, Sockets!" << std::endl;

    // What is TCP/IP
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        std::cout << "ERROR opening socket" << std::endl;

//    server = gethostbyname("www.globallogic.com");
//    if (server == NULL) {
//        std::cout << "Get host by name error!" << std::endl;
//    }

    //const char *ip = "91.202.128.77";
    const char *ip = "127.0.0.1";
    int port = 33124;

    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port=htons(port);
    serv_addr.sin_family=AF_INET;

    if (connect(sockfd, (sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "Connect error!" <<std::endl;
        return 0;
    }
    else {
        std::cout << "Connected." << std::endl;
    }

    int res = 0;
    std::string request("GET /\n\n");
    res = send(sockfd, request.c_str(), request.length(), 0);
    if(res != request.length()) {
        std::cout << "Send error!" <<std::endl;
        return 0;
    }

    std::cout << "Sent " << res << " bytes" << std::endl;

    std::string result;
    do {
        int sz = 32;
        char *data = new char[sz+1] {0};

        res = recv(sockfd, data, sz, 0);
        result+=data;

    } while(res != 0);

    std::cout << "Received:\n" << result << std::endl;

    return 0;
}