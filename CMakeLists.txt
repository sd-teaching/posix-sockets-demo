cmake_minimum_required(VERSION 3.14)
project(SocketsDemo)

set(CMAKE_CXX_STANDARD 17)

add_executable(SocketsDemo main.cpp)